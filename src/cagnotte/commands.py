import datetime
import discord

from .database import DB
from .core import CagnotteError

async def create(context, name):
    """ Create a cagnotte.

    :param context: The context of the interaction
    :type context: discord.ext.commands.Context

    :param name: Name of the cagnotte
    :type name: str
    """

    try:
        with DB() as cur:
            cur.execute("INSERT INTO cagnottes (name, guild_id, created_by) VALUES(?, ?, ?)",
                        (name, context.guild.id, context.author.id))

        await context.send(f"La cagnotte `{name}` a été créée avec succès. " +
                            "Utilisez la commande `/add` pour ajouter des utilisateur·ices.",
                           hidden = True)
    except Exception as ex:
        await context.send(f"**Erreur**: impossible de créer la cagnotte `{name}`. Raison : {ex}.",
                           hidden = True)

async def list(context):
    """ List all the available cagnotte on the server.

    :param context: The context of the interaction
    :type context: discord.ext.commands.Context
    """

    try:
        with DB() as cur:
            res = cur.execute("SELECT * FROM cagnottes WHERE guild_id = ?",
                        (context.guild.id, )).fetchall()

            if len(res) == 0:
                await context.send(f"Il n'y a aucune cagnotte d'enregistrée sur ce serveur.",
                                   hidden = True)
            elif len(res) == 1:
                await context.send(f"La cagnotte `{res[0]['name']}` (administrée par " +
                                   f"<@{res[0]['created_by']}>) est enregistrée sur ce serveur.",
                                   hidden = True)
            else:
                await context.send(f"Il y a {len(res)} cagnottes enregistrées sur ce serveur :\n" +
                                   "\n".join([f" - `{x['name']}` administrée par <@{x['created_by']}>" for x in res]),
                                   hidden = True)
    except Exception as ex:
        await context.send(f"**Erreur**: impossible d'obtenir la liste des cagnottes. Raison : {ex}.",
                           hidden = True)


async def destroy(context, cagnotte = None):
    """ Destroy a cagnotte from the server. If the cagnotte is not
    specified, and there is only one cagnotte on the server, then
    destroy this cagnotte instead.

    :param context: The context of the interaction
    :type context: discord.ext.commands.Context

    :param cagnotte: Name of the cagnotte, defaults to None.
    :type cagnotte: str | None, optional
    """

    try:
        try:
            cagnotte = get_cagnotte(context.guild.id, cagnotte)
        except CagnotteError as ex:
            await context.send(str(ex), hidden = True)
            return

        if not authorised(context.author, cagnotte):
            await context.send("Vous n'êtes pas autorisé·e à détruire cette cagnotte.", hidden = True)

        with DB() as cur:
            cur.execute("DELETE FROM cagnottes WHERE name = ? AND guild_id = ?",
                        (cagnotte, context.guild.id))

        await context.send(f"La cagnotte `{cagnotte}` a été détruite.",
                           hidden = True)
    except Exception as ex:
        await context.send(f"**Erreur**: impossible de détruire la cagnotte `{cagnotte}`. " +
                           f"Raison : {ex}.",
                           hidden = True)


async def add(context, user, max_donation = None, max_yearly = None, transfer_info = None, cagnotte = None):
    """ Add a user to a cagnotte. If the user has already been added to the cagnotte,
    this command will update its information with the information provided.

    To update a user without updating `max_donation` or `max_yearly`, set it to None.

    If no cagnotte is provided (= None), then the bot will use the bot of the server. If
    there are more than one cagnotte, an error message will be sent to the user.

    :param context: The context of the interaction
    :type context: discord.ext.commands.Context

    :param user: The user to add to the cagnotte
    :type user: discord.User

    :param max_donation: The maximum donation (in €) that this user agrees to give at once,
        defaults to None
    :type max_donation: int | None, optional

    :param max_yearly: The maximum donation (in €) that this user agrees to give per year,
        defaults to None
    :type max_yearly: int | None, optional

    :param transfer_info: Optional information about how to transfer money to that user,
        defaults to None
    :type transfer_info: str | None, optional

    :param cagnotte: The name of the cagnotte, defaults to None.
    :type cagnotte: str | None, optional
    """

    try:
        try:
            cagnotte = get_cagnotte(context.guild.id, cagnotte)
        except CagnotteError as ex:
            await context.send(str(ex), hidden = True)
            return

        with DB() as cur:
            res = cur.execute("SELECT * FROM users WHERE cagnotte_id = ? AND user_id = ? LIMIT 1",
                            (cagnotte['id'], user.id)).fetchone()

        if (not authorised(context.author, cagnotte) and
            (res is None or context.author.id != user.id)):
            await context.send("Vous n'êtes pas autorisé·e à modifier cette cagnotte.", hidden = True)
            return

        with DB() as cur:
            if res is None:
                max_donation  = 0 if max_donation  is None else max_donation
                max_yearly    = 0 if max_yearly    is None else max_yearly

                cur.execute("""INSERT INTO users
                                (cagnotte_id, user_id, max_donation, max_yearly, transfer_to)
                                VALUES (?, ?, ?, ?, ?)""",
                            (cagnotte['id'], user.id, max_donation, max_yearly, transfer_info))

                await context.send(f"L'utilisateur·ice <@{user.id}> a été ajouté·e à la cagnotte.",
                                hidden = True)
            else:
                max_donation  = res['max_donation'] if max_donation  is None else max_donation
                max_yearly    = res['max_yearly']   if max_yearly    is None else max_yearly
                transfer_info = res['transfer_to']  if transfer_info is None else transfer_info

                cur.execute("""UPDATE users
                                SET max_donation = ?, max_yearly = ?, transfer_to = ?
                                WHERE cagnotte_id = ? AND user_id = ?""",
                            (max_donation, max_yearly, transfer_info, cagnotte['id'], user.id))

                await context.send(f"Les informations de <@{user.id}> ont été mise à jour.",
                                hidden = True)
    except Exception as ex:
        await context.send(f"**Erreur**: impossible d'ajouter <@{user.id}> à la cagnotte ou de " +
                           f"mettre à jour ses informations. Raison : {ex}.",
                           hidden = True)


async def remove(context, user, cagnotte = None):
    """ Remove a user from a cagnotte.

    If no cagnotte is provided (= None), then the bot will use the bot of the server. If
    there are more than one cagnotte, an error message will be sent to the user.

    :param context: The context of the interaction
    :type context: discord.ext.commands.Context

    :param user: The user to remove from the cagnotte
    :type user: discord.User

    :param cagnotte: The name of the cagnotte, defaults to None.
    :type cagnotte: str | None, optional
    """

    try:
        try:
            cagnotte = get_cagnotte(context.guild.id, cagnotte)
        except CagnotteError as ex:
            await context.send(str(ex), hidden = True)
            return

        if context.author.id != user.id and not authorised(context.author, cagnotte):
            await context.send("Vous n'êtes pas autorisé·e à modifier cette cagnotte.", hidden = True)
            return

        with DB() as cur:
            res = cur.execute("SELECT * FROM users WHERE cagnotte_id = ? AND user_id = ? LIMIT 1",
                            (cagnotte['id'], user.id)).fetchone()

            if res is None:
                await context.send(f"L'utilisateur·ice <@{user.id}> n'est pas associé·e à la " +
                                   f"cagnotte `{cagnotte['name']}`.",
                                hidden = True)
            else:
                cur.execute("DELETE FROM users WHERE cagnotte_id = ? AND user_id = ?",
                            (cagnotte['id'], user.id))

                await context.send(f"L'utilisateur·ice <@{user.id}> a été retiré·e de la " +
                                   f"cagnotte `{cagnotte['name']}`.",
                                hidden = True)
    except Exception as ex:
        await context.send(f"**Erreur**: impossible de retirer <@{user.id}> de la cagnotte. Raison : {ex}.",
                           hidden = True)


async def use(context, user, amount, cagnotte = None):
    """ Use the cagnotte. It does not do more than printing what
    should be done to help. This method *does not* transfer money...

    If no cagnotte is provided (= None), then the bot will use the bot of the server. If
    there are more than one cagnotte, an error message will be sent to the user.

    :param context: The context of the interaction
    :type context: discord.ext.commands.Context

    :param user: Reciepient of the money
    :type user: discord.User

    :param amount: Amount requested for transfer to the user, in €
    :type amount: int

    :param cagnotte: The name of the cagnotte, defaults to None.
    :type cagnotte: str | None, optional
    """

    try:
        cagnotte = get_cagnotte(context.guild.id, cagnotte, with_report = True)
    except CagnotteError as ex:
        await context.send(str(ex), hidden = True)
        return

    if not authorised(context.author, cagnotte, with_user = True):
        await context.send("Vous n'êtes pas autorisé·e à utiliser cette cagnotte.", hidden = True)
        return

    givers          = [x for x in cagnotte['report'] if x['user_id'] != user.id]
    if len(givers) == 0:
        await context.send("Personne ne peut faire de don à <@{user.id}> depuis cette cagnotte.", hidden = True)
        return

    total_available = sum([x['balance'] for x in givers])
    total_amount    = min(amount, total_available)
    total_givable   = sum([x['max_donation'] for x in givers])

    if total_amount == 0:
        await context.send(f"La cagnotte `{cagnotte['name']}` est vide 😢", hidden = True)
        return

    msg_givable     = "que la cagnotte peut fournir"
    if total_amount < amount:
        msg_givable = f"mais la cagnotte ne peut fournir que {total_amount}€"

    try:
        rollbacks = []
        with DB(rollback = True) as cur:
            # Set the receiver to 0 (if they need money, they don't have money)
            cur.execute("""UPDATE users
                            SET last_balance = 0, last_use = CURRENT_TIMESTAMP
                        WHERE cagnotte_id = ? AND user_id = ?""",
                        (cagnotte['id'], user.id))

            transaction_id = cur.execute("""INSERT INTO transactions
                                                (cagnotte_id, amount)
                                            VALUES(?, ?)""",
                                         (cagnotte['id'], total_amount)).lastrowid

            donations = []
            for giver in givers:
                togive = int(round(total_amount * giver['max_donation'] / total_givable))
                rest   = max(0, giver['balance'] - togive)

                if togive == 0:
                    continue

                cur.execute("""UPDATE users
                                SET last_balance = ?, last_use = CURRENT_TIMESTAMP
                               WHERE cagnotte_id = ? AND user_id = ?""",
                            (rest, cagnotte['id'], giver['user_id']))

                cur.execute("""INSERT INTO history
                                   (transaction_id, from_user_id, to_user_id, amount)
                               VALUES(?, ?, ?, ?)""",
                            (transaction_id, giver['user_id'], user.id, togive))

                donations += [f"➡️  {togive}€ de la part de <@{giver['user_id']}>"]

            final_message = (f"Bonjour à vous tous·tes ! <@{user.id}> aimerait utiliser la " +
                             f"cagnotte `{cagnotte['name']}`. Iel aurait besoin de {amount}€, " +
                             f"{msg_givable}. La répartition au pro-rata des capacités de chacun·e " +
                              "est la suivante :\n" + "\n".join(donations) + "\n\nPour obtenir les " +
                              "informations pour faire le don, utilisez la commande `/transfer`.\n\n")
            await context.send(final_message)

    except Exception as ex:
        await context.send(f"**Erreur**: impossible d'utiliser la cagnotte `{cagnotte['name']}`. " +
                           f"Raison : {ex}.",
                           hidden = True)


async def status(context, cagnotte = None):
    """ Print a report to the context user about what is available for this cagnotte.

    If no cagnotte is provided (= None), then the bot will use the bot of the server. If
    there are more than one cagnotte, an error message will be sent to the user.

    :param context: The context of the interaction
    :type context: discord.ext.commands.Context

    :param cagnotte: The name of the cagnotte, defaults to None.
    :type cagnotte: str | None, optional
    """

    try:
        cagnotte = get_cagnotte(context.guild.id, cagnotte, with_report = True)
    except CagnotteError as ex:
        await context.send(str(ex), hidden = True)
        return

    if not authorised(context.author, cagnotte, with_user = True):
        await context.send("Vous n'êtes pas autorisé·e à voir cette cagnotte.", hidden = True)
        return

    if len(cagnotte['report']) == 0:
        await context.send("Il n'y a aucune personne enregistrée sur la cagnotte `{cagnotte['name']}`.", hidden = True)
        return

    try:
        donations     = [f"➡️  {x['balance']}€ (<@{x['user_id']}>, maximum {x['max_donation']}€)" for x in cagnotte['report']]
        total_amount  = sum([x['balance'] for x in cagnotte['report']])
        header = f"La cagnotte `{cagnotte['name']}` est vide :"
        if total_amount > 0:
            header = f"La cagnotte `{cagnotte['name']}` contient un total de {total_amount}€"

            if context.author.id in [x['user_id'] for x in cagnotte['report']]:
                if len(cagnotte['report']) > 1:
                    total_for_me = sum([x['balance'] for x in cagnotte['report'] if x['user_id'] != context.author.id])
                    header += f". Vous pouvez demander aux membres de la cagnotte de vous aidez à hauteur de {total_for_me}€"

            header += " :"

        final_message = "\n".join([header] + donations)
        await context.send(final_message, hidden = True)

    except Exception as ex:
        await context.send(f"**Erreur**: impossible d'utiliser la cagnotte `{cagnotte['name']}`. " +
                           f"Raison : {ex}.",
                           hidden = True)


async def transfer(context, all_transfers = False, cagnotte = None):
    """ Give some information about how to transfer the money, to whome and how much
    according to the last transaction.

    If no cagnotte is provided (= None), then the bot will use the bot of the server. If
    there are more than one cagnotte, an error message will be sent to the user.

    :param context: The context of the interaction
    :type context: discord.ext.commands.Context

    :param all_transfers: Whether to print ALL the transfers since the beginning of the cagnotte,
        defaults to False
    :type all_transfers: boolean, optional

    :param cagnotte: The name of the cagnotte, defaults to None.
    :type cagnotte: str | None, optional
    """

    try:
        cagnotte = get_cagnotte(context.guild.id, cagnotte)
    except CagnotteError as ex:
        await context.send(str(ex), hidden = True)
        return

    if not authorised(context.author, cagnotte, with_user = True):
        await context.send("Vous n'êtes pas autorisé·e à voir cette cagnotte.", hidden = True)
        return

    try:
        with DB() as cur:
            transactions = cur.execute("""SELECT history.*,
                                            transactions.cagnotte_id,
                                            transactions.amount as total_amount,
                                            transactions.date
                                        FROM transactions
                                            INNER JOIN history
                                                ON transactions.id = history.transaction_id
                                        WHERE cagnotte_id = ?
                                        ORDER BY transaction_id DESC""",
                                    (cagnotte['id'], )).fetchall()

            if len(transactions) == 0:
                await context.send(f"La cagnotte `{cagnotte['name']}` n'a jamais été utilisé.", hidden = True)
                return

            transgroup = [ [transactions[0]] ]
            for transaction in transactions[1:]:
                if transaction['transaction_id'] != transgroup[-1][0]['transaction_id']:
                    transgroup += [ [transaction] ]
                else:
                    transgroup[-1] += [transaction]

            human_date    = human_isoformat(transgroup[0][0]['date'])
            total_amount  = transgroup[0][0]['total_amount']
            final_message = (f"La cagnotte `{cagnotte['name']}` a été utilisée la dernière fois " +
                            f"le {human_date} pour un montant total de {total_amount}€. ")

            last_transaction_to   = transgroup[0][0]['to_user_id']
            if transgroup[0][0]['to_user_id'] == context.author.id:
                count = len(transgroup[0])
                if count > 1:
                    final_message += (f"Cette cagnotte a été utilisée pour vous ! {count} personnes " +
                                    "ont souhaité ou ont pu participer pour vous aider.")
                else:
                    final_message += ("Cette cagnotte a été utilisée pour vous ! Une personne a " +
                                    "souhaité ou a pu participer pour vous aider.")
            else:
                last_transaction_user = [x for x in transgroup[0] if x['from_user_id'] == context.author.id]
                if len(last_transaction_user) == 0:
                    final_message += "Vous n'avez pas participé à cette cagnotte."
                else:
                    transfer_info = cur.execute("SELECT * FROM users WHERE user_id = ?",
                                                (last_transaction_to, )).fetchone()

                    if transfer_info is None or transfer_info['transfer_to'] is None:
                        transfer_info = f"Contactez <@{last_transaction_to}> directement."
                    else:
                        transfer_info = transfer_info['transfer_to']

                    last_transaction_amount = last_transaction_user[0]['amount']
                    final_message += (f"Votre transfert à <@{last_transaction_to}> est de " +
                                      f"{last_transaction_amount}€.\n\n" +
                                      f"💸 **Information sur le transfert** 💸\n{transfer_info}\n\n" +
                                      "> ⚠️ Rappel que ce don n'est pas une obligation, mais un " +
                                      "engagement moral. Selon votre situation au moment de la " +
                                      "demande de transfert, vous ne serez peut-être pas en mesure " +
                                      "de donner. N'oubliez jamais que vous pouvez **toujours** " +
                                      "refuser de faire le don, sans avoir à vous justifier.")

            if all_transfers and len(transgroup) == 1:
                final_message += "\n\nIl n'y a pas eu d'autres transfert avant celui là."
            elif all_transfers:
                if len(transgroup) == 2:
                    final_message += "\n\n🕣 **Transfert antérieur** 🕣\n"
                else:
                    final_message += "\n\n🕣 **Transferts antérieurs** 🕣\n"

                transmessages = []
                for transaction in transgroup[1:]:
                    total_amount = transaction[0]['total_amount']
                    human_date   = human_isoformat(transaction[0]['date'])
                    to_user      = transaction[0]['to_user_id']
                    user_transac = [x for x in transaction if x['from_user_id'] == context.author.id]

                    if to_user == context.author.id:
                        message = f"➖ {human_date} : **vous** avez reçu une promesse de don de {total_amount}€"
                    else:
                        message = f"➖ {human_date} : <@{to_user}> a reçu une promesse de don de {total_amount}€"
                        if len(user_transac) == 0:
                            message += " à laquelle vous n'avez pas participé."
                        else:
                            message += f" dont {user_transac[0]['amount']}€ de votre part."

                    transmessages += [message]

                final_message += "\n".join(transmessages)

        await context.send(final_message, hidden = True)

    except Exception as ex:
        await context.send(f"**Erreur**: impossible d'utiliser la cagnotte `{cagnotte['name']}`. " +
                           f"Raison : {ex}.",
                           hidden = True)
        raise ex



def get_cagnotte(guild_id, name, with_report = False):
    """ Gather the information from the database about a cagnotte.

    :param guild_id: The server ID to which the cagnotte belongs
    :type guild_id: int

    :param name: The name of the cagnotte
    :type name: str

    :param with_report: Whether to return a key ``report`` containing information about
        the available money and the possible user contributions, defaults to False
    :type with_report: bool, optional

    :return: A dictionary with all the information about a cagnotte, possibly with
        a key ``report`` containing the information about the money available so far. The
        dictionary contains multiple self-explenatory keys.
    :rtype: dict[Any]
    """

    with DB() as cur:
        if name is None:
            cagnottes = cur.execute("SELECT * FROM cagnottes WHERE guild_id = ?",
                                    (guild_id, )).fetchall()

            if len(cagnottes) == 0:
                raise CagnotteError("Il n'y a aucune cagnotte d'enregistrée sur ce serveur.")

            elif len(cagnottes) > 1:
                raise CagnotteError(f"Il y a {len(cagnottes)} enregistrées sur ce serveur, veuillez " +
                                     "préciser le nom de la cagnotte à laquelle ajouter un·e utilisateur·ice.")

            cagnotte = cagnottes[0]
        else:
            res = cur.execute("SELECT id,created_by FROM cagnottes WHERE name = ? AND guild_id = ?",
                            (name, guild_id)).fetchone()

            if res is None:
                raise CagnotteError(f"Il n'y a aucune cagnotte `{name}` enregistrée sur ce serveur.")

            cagnotte = res

        report = None
        if with_report:
            now    = datetime.datetime.now(datetime.timezone.utc)
            users  = cur.execute("SELECT * FROM users WHERE cagnotte_id = ?",
                                (cagnotte['id'], )).fetchall()
            report = []
            for user in users:
                last_use     = datetime.datetime.fromisoformat(user['last_use'] + "+00:00")
                last_balance = user['last_balance']
                rate         = user['max_yearly'] / 365.25

                duration     = (now - last_use).total_seconds() / 86400
                balance      = last_balance + rate * duration
                balance      = int(round(min(balance, user['max_donation'])))

                report += [{"user_id": user['user_id'], "balance": balance,
                            "max_donation": user['max_donation'],
                            "max_yearly": user['max_yearly']}]

        return {**cagnotte, "report": report}


def authorised(user, cagnotte = None, with_user = False):
    """ Checks whether a user is authorised to manage a cagnotte.

    If no cagnotte is provided (= None), then the bot will use the bot of the server. If
    there are more than one cagnotte, an error message will be sent to the user.

    :param user: Which user authorisation should be tested
    :type user: discord.User

    :param cagnotte: The name of the cagnotte, defaults to None.
    :type cagnotte: str | None, optional

    :param with_user: Whether to authorised user of the cagnotte, defaults to False
    :type with_user: bool, optional

    :return: Whether the ``user`` is authorised to manage the cagnotte
    :rtype: bool
    """

    created_by = None if cagnotte is None else cagnotte['created_by']
    manager    = user.guild_permissions.manage_guild

    user_ids   = (created_by, )
    if with_user and cagnotte is not None:
        with DB() as cur:
            users = cur.execute("SELECT * FROM users WHERE cagnotte_id = ?",
                                (cagnotte['id'], )).fetchall()
            user_ids = (*user_ids, *[x['user_id'] for x in users])

    return manager or user.id in user_ids

def human_isoformat(date):
    """ Convert isoformat datetime  to a human readable date.

    :param date: The date in the isoformat "yyyy-mm-dd HH:MM:SS"
    :type date: str

    :return: A human readable date
    :rtype: str
    """

    dt = datetime.datetime.fromisoformat(date + "+00:00")
    timestamp = int(round(dt.timestamp()))
    return f"<t:{timestamp}>"
