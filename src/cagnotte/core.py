import discord
from . import database

class Cagnotte(discord.ext.commands.Bot):
    """ The Discord Bot to manage Cagnotte.

    :param dbpath: Filepath to the SQLite database
    :type dbpath: str
    """

    def __init__(self, dbpath):
        super().__init__(command_prefix = "418652e1d53f4f9d2ec4734c87696544")
        self.initalized = False
        self.dbpath     = dbpath

    async def on_ready(self):
        """ Run when the bot is ready to serve. """
        database.init(self.dbpath)
        self.initalized = True
        print("Bot initialized")


class CagnotteError(Exception):
    """ Special error specific to the Cagnotte Bot.

    :param reason: The reason for using an error
    :type reason: str
    """

    def __init__(self, reason):
        super().__init__(reason)
