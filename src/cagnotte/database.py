import sqlite3

class DB:
    """ Fast connection to the database using context management.

    If no ``dbpath`` is provided, the class property ``DB.DEFAULT_DBPATH`` will be used
    with no guarantee (by default, ``DB.DEFAULT_DBPATH`` is set to ``None`` and must be
    changed to fit with your application).

    :param dbpath: Filepath to the sqlite database, defaults to None
    :type dbpath: str | None, optional

    :param rollback: Whether to rollback automatically when an error occurs, defaults to False
    :type rollback: bool, optional
    """

    DEFAULT_DBPATH = None

    def __init__(self, dbpath = None, rollback = False):
        self.dbpath   = DB.DEFAULT_DBPATH if dbpath is None else dbpath
        self.rollback = rollback

    def __enter__(self):
        self.conn = sqlite3.connect(self.dbpath)
        self.conn.row_factory = sqlite3.Row

        cursor = self.conn.cursor()
        cursor.execute("PRAGMA foreign_keys = ON")

        return cursor

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None and self.rollback:
            self.conn.rollback()
        else:
            self.conn.commit()

        self.conn.close()


def init(dbpath):
    """ Initialise the database if it does not exist yet

    :param dbpath: Filepath to the sqlite database
    :type dbpath: str
    """

    with DB(dbpath) as cursor:
        cursor.execute('''CREATE TABLE IF NOT EXISTS cagnottes (
                            id         INTEGER          PRIMARY KEY AUTOINCREMENT,
                            name       VARCHAR (1, 255) NOT NULL,
                            guild_id   INTEGER          NOT NULL,
                            created_by INTEGER          NOT NULL,
                            created_at DATETIME         NOT NULL DEFAULT (CURRENT_TIMESTAMP));''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS users (
                                id           INTEGER  PRIMARY KEY AUTOINCREMENT,
                                cagnotte_id  INTEGER  NOT NULL REFERENCES cagnottes (id) ON DELETE CASCADE,
                                user_id      INTEGER  NOT NULL,
                                max_donation INTEGER  NOT NULL CHECK (max_donation >= 0),
                                max_yearly   INTEGER  NOT NULL CHECK (max_yearly >= 0),
                                last_balance INTEGER  NOT NULL CHECK (last_balance >= 0) DEFAULT (0),
                                last_use     DATETIME NOT NULL DEFAULT (CURRENT_TIMESTAMP),
                                transfer_to  TEXT);''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS transactions (
                            id          INTEGER  PRIMARY KEY AUTOINCREMENT,
                            cagnotte_id INTEGER  NOT NULL REFERENCES cagnottes (id),
                            amount      INTEGER  NOT NULL CHECK (amount >= 0),
                            date        DATETIME NOT NULL DEFAULT (CURRENT_TIMESTAMP));''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS history (
                            id             INTEGER  PRIMARY KEY AUTOINCREMENT,
                            transaction_id INTEGER  NOT NULL REFERENCES transactions (id),
                            from_user_id   INTEGER  NOT NULL,
                            to_user_id     INTEGER  NOT NULL,
                            amount         INTEGER  NOT NULL CHECK (amount >= 0));''')

        DB.DEFAULT_DBPATH = dbpath

