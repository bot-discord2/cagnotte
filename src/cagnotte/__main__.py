import argparse

import discord
import discord_slash
import os
import sys

from . import commands
from .core import Cagnotte

parser = argparse.ArgumentParser()
parser.add_argument("database", help = "Path to the database file")
args = parser.parse_args()

TOKEN = os.environ.get('TOKEN', None)
if TOKEN is None:
    print("You must define the environmental variable TOKEN", file = sys.stderr)
    sys.exit(1)

cagnotte = Cagnotte(args.database)
slash    = discord_slash.SlashCommand(cagnotte, sync_commands = True)


@slash.slash(name = "create",
             description = "Créer une nouvelle cagnotte",
             options = [
                discord_slash.utils.manage_commands.create_option(
                    name        = "name",
                    description = "Nom de la cagnotte",
                    option_type = discord_slash.model.SlashCommandOptionType.STRING,
                    required    = True)])
@discord.ext.commands.guild_only()
async def _create(ctx, name):
    await commands.create(ctx, name)


@slash.slash(name = "list",
             description = "Lister les cagnottes du serveur")
@discord.ext.commands.guild_only()
async def _list(ctx):
    await commands.list(ctx)


@slash.slash(name = "destroy",
             description = "Supprimer une cagnotte du serveur",
             options = [
                discord_slash.utils.manage_commands.create_option(
                    name        = "cagnotte",
                    description = "Nom de la cagnotte",
                    option_type = discord_slash.model.SlashCommandOptionType.STRING,
                    required    = True)])
@discord.ext.commands.guild_only()
async def _destroy(ctx, cagnotte):
    await commands.destroy(ctx, cagnotte)


@slash.slash(name = "add",
             description = "Ajouter une personne à une cagnotte ou mettre à jour ses informations",
             options     = [
                discord_slash.utils.manage_commands.create_option(
                    name        = "user",
                    description = "La personne à ajouter",
                    option_type = discord_slash.model.SlashCommandOptionType.USER,
                    required    = True),

                discord_slash.utils.manage_commands.create_option(
                    name        = "max_per_donation",
                    description = "Don maximum en une fois",
                    option_type = discord_slash.model.SlashCommandOptionType.INTEGER,
                    required    = False),

                discord_slash.utils.manage_commands.create_option(
                    name        = "max_per_year",
                    description = "Don maximum par an",
                    option_type = discord_slash.model.SlashCommandOptionType.INTEGER,
                    required    = False),

                discord_slash.utils.manage_commands.create_option(
                    name        = "transfer_info",
                    description = "Information pour le transfert d'argent (plateforme de don, RIB, ...)",
                    option_type = discord_slash.model.SlashCommandOptionType.STRING,
                    required    = False),

                discord_slash.utils.manage_commands.create_option(
                    name        = "cagnotte",
                    description = "Le nom de la cagnotte s'il y en a plusieurs.",
                    option_type = discord_slash.model.SlashCommandOptionType.STRING,
                    required    = False)])
@discord.ext.commands.guild_only()
async def _add(ctx, user, max_per_donation = None, max_per_year = None,
               transfer_info = None, cagnotte = None):
    await commands.add(ctx, user, max_per_donation, max_per_year, transfer_info, cagnotte)


@slash.slash(name = "remove",
             description = "Retirer une personne d'une cagnotte",
             options = [
                discord_slash.utils.manage_commands.create_option(
                    name        = "user",
                    description = "La personne à retirer",
                    option_type = discord_slash.model.SlashCommandOptionType.USER,
                    required    = True),

                discord_slash.utils.manage_commands.create_option(
                    name        = "cagnotte",
                    description = "Le nom de la cagnotte s'il y en a plusieurs.",
                    option_type = discord_slash.model.SlashCommandOptionType.STRING,
                    required    = False)])
@discord.ext.commands.guild_only()
async def _remove(ctx, user, cagnotte = None):
    await commands.remove(ctx, user, cagnotte)


@slash.slash(name = "use",
             description = "Utiliser l'argent d'une cagnotte",
             options = [
                discord_slash.utils.manage_commands.create_option(
                    name        = "user",
                    description = "Nom de l'utilisateur·ice",
                    option_type = discord_slash.model.SlashCommandOptionType.USER,
                    required    = True),

                discord_slash.utils.manage_commands.create_option(
                    name        = "amount",
                    description = "Somme reversée",
                    option_type = discord_slash.model.SlashCommandOptionType.INTEGER,
                    required    = True),

                discord_slash.utils.manage_commands.create_option(
                    name        = "cagnotte",
                    description = "Nom de la cagnotte s'il y en a plusieurs",
                    option_type = discord_slash.model.SlashCommandOptionType.STRING,
                    required    = False)])
@discord.ext.commands.guild_only()
async def _use(ctx, user, amount, cagnotte = None):
    await commands.use(ctx, user, amount, cagnotte)


@slash.slash(name = "status",
             description = "Afficher l'état actuel d'une cagnotte",
             options = [
                discord_slash.utils.manage_commands.create_option(
                    name        = "cagnotte",
                    description = "Nom de la cagnotte s'il y en a plusieurs",
                    option_type = discord_slash.model.SlashCommandOptionType.STRING,
                    required    = False)])
@discord.ext.commands.guild_only()
async def _status(ctx, cagnotte = None):
    await commands.status(ctx, cagnotte)


@slash.slash(name = "transfer",
             description = "Donne des informations sur les transfers fait ou à faire",
             options = [
                discord_slash.utils.manage_commands.create_option(
                    name        = "all_transfers",
                    description = "Afficher tous les transferts d'argent",
                    option_type = discord_slash.model.SlashCommandOptionType.BOOLEAN,
                    required    = False),

                discord_slash.utils.manage_commands.create_option(
                    name        = "cagnotte",
                    description = "Nom de la cagnotte s'il y en a plusieurs",
                    option_type = discord_slash.model.SlashCommandOptionType.STRING,
                    required    = False)])
@discord.ext.commands.guild_only()
async def _transfer(ctx, all_transfers = False, cagnotte = None):
    await commands.transfer(ctx, all_transfers, cagnotte)


cagnotte.run(TOKEN)
